export interface LogContent {
    m: string,
    level: string,
    data?: any[],
}
